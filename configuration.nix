# main config

{ config, pkgs, ... }:

{
    imports =
        [ # Include the results of the hardware scan.
        ./hardware-configuration.nix
        ];

# grub config
    boot.loader.systemd-boot.enable = false;
    boot.loader = {
        efi = {
            canTouchEfiVariables = true;
            efiSysMountPoint = "/boot";
        };
        timeout = -1;
        grub = {
            device = "nodev";
            efiSupport = true;
            useOSProber = true;

# grub theme config
        theme = pkgs.stdenv.mkDerivation {
            name = "bigsur-grub2-theme";
            src = builtins.fetchGit {
		url = "https://github.com/Teraskull/bigsur-grub2-theme";
		ref = "master";
            };
            installPhase = "cp -r bigsur $out";
            gfxmodeEfi = "1920x1080";
            gfxmodeBios = "1920x1080";
        };
    };

    };
    boot.kernelParams = [ "resume_offset=18075648" "i8042.nopnp=1" "i8042.dumbkbd=1"];
    boot.resumeDevice = "/dev/disk/by-label/NIXROOT";

    networking.hostName = "iuseotherswifi"; # Define your hostname.
    networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

# Set your time zone.
    time.timeZone = "Asia/Kolkata";

# Installing fonts from nerdfonts
    fonts.fonts = with pkgs; [
        (nerdfonts.override { fonts = [ "FiraCode" ]; })
    ];

# Hiberante
    services.logind = {
        extraConfig = "HandlePowerKey=hibernate";
        lidSwitch = "hibernate";
    };

# autologin
    services.getty.autologinUser = "notpua";

# pipewire
    services.pipewire = {
        enable = true;
        alsa.enable = true;
        pulse.enable = true;
    };

# allow unfree packages
    nixpkgs.config.allowUnfree = true;

# Define a user account.
    programs.zsh.enable = true;
    users.users.notpua = {
        initialPassword = "notpua";
        isNormalUser = true;
        shell = pkgs.zsh;
        extraGroups = [ "wheel" "audio" "video" "networkmanager" "nm-openvpn" "disk"]; # Enable ‘sudo’ for the user.
        packages = with pkgs; [
          brightnessctl
          pavucontrol
          vscode
        ];
    };

# backup bash just in case
    environment.shells = with pkgs; [ zsh bash ];

# Security fixes with linux
    security.doas.enable = true;
    security.sudo.enable = false;
    security.doas.extraRules = [{
      users = [ "notpua" ];
      keepEnv = true;
      noPass = true;
    }];
    security.polkit.enable = true;

# enable opengl for wayland usage
    hardware.opengl = {
      enable = true;
      driSupport = true;
    };

# enabling portals
    services.dbus.enable = true;
    xdg.portal = {
      enable = true;
      wlr.enable = true;
      extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };


# bluetooth support with blueman
    hardware.bluetooth.enable = true;
    services.blueman.enable = true;

# List packages installed in system profile. To search, run:
# $ nix search wget
    environment.systemPackages = with pkgs; [
      ((vim_configurable.override { python3 = python3; }   ))
      wget
      git
      pulseaudio

      # latex
      ( texlive.combine {
        inherit (texlive) scheme-small titlesec;
      })

      # rust compilation
      rustc cargo
      # node and npm
      nodejs yarn
      # python and modules
      (python311.withPackages(ps: with ps; [
        pandas
        requests
        numpy sympy
      ]))
    ];

# save configuration
    system.copySystemConfiguration = true;

# nix flakes
    nix = {
        extraOptions = "experimental-features = nix-command flakes";
        package = pkgs.nixFlakes;
    };
    system.stateVersion = "22.11";

}
