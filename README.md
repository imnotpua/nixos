# Nixos

There are two parts to this Nixos setup: Global (configuration.nix) and the local (home.nix) and related files.

It is recommended to use symlinks to avoid repeated copying. Make sure your `configuration.nix` and `hardware-configuration.nix` has read permissions for all users.

Force symlink, overwriting the current dotfiles with the present ones using the command:
```# ln -sf $(pwd)/configuration.nix /etc/nixos/configuration.nix```
```# ln -sf $(pwd)/hardware-configuration.nix /etc/nixos/hardware-configuration.nix```

For the home-manager files you already have the read permissions if it applies to the same user. So just symlink all the files inside the home-manager folder one-by-one
```$ ln -sf $(pwd)/home-manager/* $HOME/.config/nixpkgs/```

