{ inputs, lib, config, pkgs, ... }:

{

    imports = [
        ./zsh.nix
        ./sway.nix
        ~/.config/home-manager/personal.nix
    ];

    nixpkgs = {
        config = {
            allowUnfree = true;
            allowUnfreePredicate = (_: true);
        };
    };

    home = {
        username = "notpua";
        homeDirectory = "/home/notpua";

    };

    home.packages = with pkgs; [
        mullvad-browser
        masterpdfeditor4
        zathura
        # build tools
        gnumake clang cmake ruby go python3 jdk17 rustup
    ];

# Enable home-manager and git
    programs.home-manager.enable = true;
    programs.git = {
      enable = true;
      userName = "notpua";
      userEmail = "atp@tutamail.com";
      includes = [{ path = "~/.config/git/personal"; }];
    };

# Nicely reload system units when changing configs
    systemd.user.startServices = "sd-switch";
    home.stateVersion = "23.05";

# configs here
    programs.qutebrowser = {
        enable = true;
        searchEngines = {
            DEFAULT = "https://startpage.com/search?q={}";
        };
        settings = {
            colors.tabs.bar.bg = "#ffffff";
            tabs.position = "left";
            url.default_page = "https://startpage.com";
            url.start_pages = "https://login.live.com";
        };
    };

# vimrc file import
    xdg.configFile."vim/vimrc".source = builtins.fetchGit {
	url = "https://gitlab.com/imnotpua/dotfiles";
        ref = "main";
    } + "/vim/vimrc";

    xdg.configFile."vim/gvimrc".source = builtins.fetchGit {
	url = "https://gitlab.com/imnotpua/dotfiles";
        ref = "main";
    } + "/vim/gvimrc";


# alacritty file inport
    xdg.configFile."alacritty/alacritty.yml".source = builtins.fetchGit {
	url = "https://gitlab.com/imnotpua/dotfiles";
        ref = "main";
    } + "/terminal-emulators/alacritty/alacritty.yml";

# zathura support
    programs.zathura = {
	enable = true;
	options = {
	    default-bg = "#ffffff";	
	    default-fg = "#000000";
	};
    };
}
