{ pkgs }:

{

### Keybindings

    "Mod1+d" = "exec dmenu-wl_run -fn 'FiraCode Nerd Font' -nb '#ffffff' -nf '#000000' -sb '#dddddd' -sf  '#000000' -h 18 | xargs swaymsg exec --";
    "Mod1+b" = "exec mullvad-browser";
    "Mod4+n" = "exec 'alacritty -e nmtui'";
    "Mod4+a" = "exec pavucontrol";
    "Mod1+t" = "exec alacritty";
    "Mod4+b" = "exec blueman-manager";
    "Mod4+v" = "exec copyq toggle";

    # action keys
    "Mod1+Shift+q" = "kill";
    "Mod1+Shift+r" = "reload";
    "Mod1+Shift+c" =  "exec swaymsg exit";

# Moving around:

    # Move your focus around
    "Mod1+j" =  "focus left";
    "Mod1+k" = "focus right";
    "Mod1+Up" = "focus up";
    "Mod1+Down" = "focus down";

    # Move the focused window with the same, but add Shift
    "Mod1+Shift+h" = "move left";
    "Mod1+Shift+j" = "move down";
    "Mod1+Shift+k" = "move up";
    "Mod1+Shift+l" = "move right";

    "Mod1+l" = "resize shrink width 1 px or 1 ppt";
    "Mod1+h" = "resize grow width 1 px or 1 ppt";

# Workspaces:

    # Switch to workspace
    "Mod1+1" = "workspace number 1";
    "Mod1+2" = "workspace number 2";
    "Mod1+3" = "workspace number 3";
    "Mod1+4" = "workspace number 4";

   # Move focused container to workspace
    "Mod1+Shift+1" = "move container to workspace number 1";
    "Mod1+Shift+2" = "move container to workspace number 2";
    "Mod1+Shift+3" = "move container to workspace number 3";
    "Mod1+Shift+4" = "move container to workspace number 4";

# Layout stuff:

    "Mod4+j" = "splitv";
    "Mod4+l" = "splith";

    # switching layouts
    "Mod1+x" = "layout stacking";
    "Mod1+z" = "layout tabbed";
    "Mod1+c" = "layout floating toggle";

    "Mod1+f" = "fullscreen";
    "Mod1+tab" = "focus mode_toggle";

    # Move focus to the parent container
    "Mod1+a" = "focus parent";

# killing waybar and spawning
    "Mod4+tab" = "exec killall -SIGUSR1 .waybar-wrapped";

# requires grim, slurp and wl-copy
    "Print" =  "exec grim -g '$(slurp)' - | wl-copy; exec grim $HOME/screenshots/$(date +'%A_%H:%M_%p_%d-%m-%Y.png')";


# requires Mako
    "Mod1+Ctrl+n" = "exec makoctl dismiss";
    "Mod1+Shift+n" = "exec makoctl dismiss -a";

# volume / brightness / media controls   ## needs pactl and brightnessctl
    "XF86AudioRaiseVolume" = "exec pactl set-sink-volume @DEFAULT_SINK@ +1%";
    "XF86AudioLowerVolume" = "exec pactl set-sink-volume @DEFAULT_SINK@ -1%";
    "XF86AudioMute" = "exec pactl set-sink-mute @DEFAULT_SINK@ toggle";
    "XF86AudioMicMute" = "exec pactl set-source-mute @DEFAULT_SOURCE@ toggle";
    "XF86MonBrightnessDown" = "exec brightnessctl set 1%-";
    "XF86MonBrightnessUp" = "exec brightnessctl set +1%";
    "XF86AudioPlay" = "exec playerctl play-pause";
    "XF86AudioNext" = "exec playerctl next";
    "XF86AudioPrev" = "exec playerctl previous";

    "Mod1+r" = "mode 'resize'";

    "Mod1+Mod4+left" = "exec busctl --user -- call rs.wl-gammarelay / rs.wl.gammarelay UpdateTemperature n -500";
    "Mod1+Mod4+right" = "exec busctl --user call rs.wl-gammarelay / rs.wl.gammarelay UpdateTemperature n 500";
    "Mod4+down" = "exec busctl --user -- call rs.wl-gammarelay / rs.wl.gammarelay UpdateBrightness d -0.1";
    "Mod4+up" = "exec busctl --user call rs.wl-gammarelay / rs.wl.gammarelay UpdateBrightness d 0.1";
    "Mod4+left" = "exec busctl --user -- call rs.wl-gammarelay / rs.wl.gammarelay UpdateGamma d -0.1";
    "Mod4+right" = "exec busctl --user call rs.wl-gammarelay / rs.wl.gammarelay UpdateGamma d 0.1";
    "Mod4+Ctrl+c" = "exec busctl --user call rs.wl-gammarelay / rs.wl.gammarelay ToggleInverted";
    "Mod4+Ctrl+v" = "exec busctl --user set-property rs.wl-gammarelay / rs.wl.gammarelay Brightness d 1";
    "Mod4+Ctrl+x" = "exec busctl --user set-property rs.wl-gammarelay / rs.wl.gammarelay Brightness d 0.3";
    "Mod4+BackSpace" = "exec busctl --user set-property rs.wl-gammarelay / rs.wl.gammarelay Brightness d 1; exec busctl --user set-property rs.wl-gammarelay / rs.wl.gammarelay Gamma d 1; exec busctl --user set-property rs.wl-gammarelay / rs.wl.gammarelay Temperature q 5000";

}
