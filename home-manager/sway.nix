 { libs, pkgs, config, options, ...}:

# alternate for session vairables, from official docs

let

  dbus-sway-environment = pkgs.writeTextFile {
    name = "dbus-sway-environment";
    destination = "/bin/dbus-sway-environment";
    executable = true;

    text = ''
      dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway
      systemctl --user stop pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
      systemctl --user start pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
    '';
  };

  configure-gtk = pkgs.writeTextFile {
    name = "configure-gtk";
    destination = "/bin/configure-gtk";
    executable = true;
    text = let
      schema = pkgs.gsettings-desktop-schemas;
      datadir = "${schema}/share/gsettings-schemas/${schema.name}";
    in ''
      export XDG_DATA_DIRS=${datadir}:$XDG_DATA_DIRS
      gnome_schema=org.gnome.desktop.interface
      gsettings set $gnome_schema gtk-theme 'Dracula'
    '';
  };

in
{
    home.packages = with pkgs; [
        configure-gtk
        mullvad-browser
        grim slurp
        wl-clipboard
        mako
        brightnessctl
        dmenu-wayland
        dbus-sway-environment
	    copyq
	    killall # for waybar
    ];

# zsh sway autostart
 programs = {
   zsh = {
     initExtra = ''
       if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
          exec sway --config ~/.config/sway/config
       fi
     '';
   };
 };

###########################################################
#                                            / _(_)       #
#  _____      ____ _ _   _    ___ ___  _ __ | |_ _  __ _  #
# / __\ \ /\ / / _` | | | |  / __/ _ \| '_ \|  _| |/ _` | #
# \__ \\ V  V / (_| | |_| | | (_| (_) | | | | | | | (_| | #
# |___/ \_/\_/ \__,_|\__, |  \___\___/|_| |_|_| |_|\__, | #
#                     __/ |                         __/ | #
#                    |___/                         |___/  #
###########################################################

  wayland.windowManager.sway = {
    enable = true;
    wrapperFeatures.gtk = true;

    config = {
      fonts = {
        names = [ "FiraCode" ];
        size = 16.0;
      };

      # modifier key
      modifier = "Mod1";

      # wallpaper
      output = {
        "*" = {
            bg = "~/screenshots/white.png fill";
        };
      };

      # input keyboard and touchpad
      input = {
        "type:keyboard" = {
            xkb_options = "caps:swapescape";
            repeat_rate = "65";
            repeat_delay = "150";
        };
        "type:touchpad" = {
            dwt = "enabled";
            tap = "enabled";
            natural_scroll = "enabled";
            middle_emulation = "enabled";
        };
      };

      # focus mouse
      focus.followMouse = true;

      # statusbars
      bars = [{
        command = "waybar";
        hiddenState = "hide";
        mode = "hide";
        position = "top";
      }];

      # remove borders
      window.border = 0;
      window.hideEdgeBorders = "both";
      window.titlebar = false;
      floating.border = 1;
      floating.titlebar = false;

      terminal = "alacritty";

      # autostart apps
      startup = [
        {command = "mako"; always = true;}
        {command = "wl-gammarelay-rs"; always = true;}
        {command = "copyq"; always = true;}
      ];

      # keybindings
      keybindings = import ./sway_keybindings.nix { pkgs = pkgs; };

      # resizing
      modes = {
        resize = {
          h = "resize shrink width 10 px";
          j = "resize grow height 10 px";
          k = "resize shrink height 10 px";
          l = "resize grow width 10 px";
        };
      };
    };

    # swaynag

    swaynag = {
        enable = true;
        settings = {
            "" = {
              edge = "top";
              font = "FiraCode 12";
            };

            green = {
              edge = "top";
              background = "ffffff";
              text = "000000";
              button-background = "dddddd";
              details-background = "eeeeee";
              message-padding = 10;
            };
        };
    };

  };


############################################################################
# __          __         _                   _____             ___         #
# \ \        / /        | |                 / ____|           / _(_)       #
#  \ \  /\  / /_ _ _   _| |__   __ _ _ __  | |     ___  _ __ | |_ _  __ _  #
#   \ \/  \/ / _` | | | | '_ \ / _` | '__| | |    / _ \| '_ \|  _| |/ _` | #
#    \  /\  / (_| | |_| | |_) | (_| | |    | |___| (_) | | | | | | | (_| | #
#     \/  \/ \__,_|\__, |_.__/ \__,_|_|     \_____\___/|_| |_|_| |_|\__, | #
#                   __/ |                                            __/ | #
#                  |___/                                            |___/  #
############################################################################

    programs.waybar = {
      enable = true;

      settings = [{
        layer = "top";
        position = "top";
        modules-left = ["sway/workspaces"];
        modules-center = ["sway/window"];
        modules-right = ["battery" "clock"];
        clock.format = "{:%I:%M %p  |  %A  |  %e %B %Y }";
        battery = {
            states = {
                good = 95;
                warning = 30;
                critical = 15;
            };
            format =  "    | {capacity}% {icon} | ";
            format-charging =  "    | {capacity}%  ⚡ | ";
            format-plugged =  "     | {capacity}%  ⚡ | ";
	        format-icons =  ["!" "" "" "" "!"];
	    };
	    "sway/workspaces" = {
	        on-click = "activate";
	        all-outputs = true;
	        format = " {name} |";
        };
	}];

        style = ''
            * {
                border: none;
                border-radius: 0;
                font-family: "FiraCode Nerd Font","Noto Color Emoji", sans-serif;
                font-size: 24px;
                color: #000000;
                background: #ffffff;
            }

	    window#waybar {
		padding-left: 100px;
		padding-right: 100px;
	    }

	    #workspaces {
		padding-right: 100px;
	    }

            #workspaces button {
                padding-left: 0px;
                padding-right: 0px;
                background-color: #ffffff;
                color: #ffffff;
            }

            #workspaces button.active {
                background-color: #dddddd;
		font-weight: bold;
            }

	    #workspaces button.focused {
                background-color: #dddddd;
            }

            #workspaces button.urgent {
                background: #bf616a;
            }

            @keyframes blink {
                to {
                    /*background-color: #ffffff;*/
                    color: #000000;
                }
            }

            #battery.critical:not(.charging) {
                background: #f53c3c;
                color: #ffffff;
                animation-name: blink;
                animation-duration: 0.5s;
                animation-timing-function: linear;
                animation-iteration-count: infinite;
                animation-direction: alternate;
                font-family: "FiraCode"
            }

        '';

    };

}
