{ config, pkgs, options, ... }: {


# autojump integration
    programs.autojump = {
        enable = true;
        enableZshIntegration = true;
    };

# required packages for zsh
    home.packages = with pkgs; [
        htop
        neofetch
    ];

    programs.zsh = {

        enable = true;
        dotDir = ".config/zsh";

        # settings the session variables
        sessionVariables = {

            BROWSER = "mullvad-browser";
            EDITOR = "vim";
            VISUAL = "vim";

            VI_MODE_SET_CURSOR=true;
            VI_MODE_CURSOR_NORMAL=1;
            VI_MODE_CURSOR_VISUAL=4;
            VI_MODE_CURSOR_INSERT=5;
            VI_MODE_CURSOR_OPPEND=0;

            VIMINIT="source ~/.config/vim/vimrc";
            GVIMINIT="source ~/.config/vim/gvimrc";
            NODE_PATH="~/.config/npm/node_modules/";
            CARGO_HOME="~/.config/cargo";
            GIT_CONFIG_GLOBAL="~/.config/git/config";

            PATH="$PATH:$HOME/.config/npm/bin:$HOME/.config/cargo/bin:$HOME/.local/bin";
        };

        # plugins
        historySubstringSearch.enable = true;
        history = {
            extended = true;
            path = "${config.xdg.dataHome}/zsh/zsh_history";
        };

        shellAliases = {
            cp="cp -i";
            mv="mv -i";
            mkdir="mkdir -p";
            ps="ps auxf";
            ping="ping -c 10";
            less="less -R";
            cls="clear";
            dvi="doas vim";
            l="ls -al";
            sudo="doas";
        };

        oh-my-zsh = {
            enable = true;
            custom = "${(builtins.fetchGit {
		url = "https://gitlab.com/imnotpua/comfyline_prompt";
                ref = "main";
            })}";
            theme = "comfyline";
        };

        zplug = {
            enable = true;
            plugins = [
            { name = "zsh-users/zsh-syntax-highlighting"; }
            { name = "Nyquase/vi-mode"; }
            { name = "zsh-users/zsh-completions"; }
            { name = "zsh-users/zsh-autosuggestions"; }
            ];
            zplugHome = "${config.xdg.configHome}/zplug";
        };

        initExtra = ''
[[ -f ~/.config/zsh/personalrc ]] && source ~/.config/zsh/personalrc

# syntax highlighting stuff
    typeset -A ZSH_HIGHLIGHT_STYLES
    ZSH_HIGHLIGHT_STYLES[alias]='fg=magenta'
    ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=red'
    ZSH_HIGHLIGHT_STYLES[precommand]='fg=#23f93d'
    ZSH_HIGHLIGHT_STYLES[command]='fg=#23f93d,bold'
    ZSH_HIGHLIGHT_STYLES[path]='fg=#24eef9'
    ZSH_HIGHLIGHT_STYLES[globbing]='fg=#24eef9'

# defaultKeymap doesn't work
            bindkey -v
'';

    };
}
